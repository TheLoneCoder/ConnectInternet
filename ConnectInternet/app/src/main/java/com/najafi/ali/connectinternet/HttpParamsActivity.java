package com.najafi.ali.connectinternet;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class HttpParamsActivity extends AppCompatActivity {

    public static final String URI_SHOW_PARAMS = "http://goodnajafiboy.000webhostapp.com/mysite/showparams.php";
    ProgressBar pb;
    TextView tv;
    List<AsyncTask> tasks = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_http_parameters);
        tv = findViewById(R.id.tv);
        pb = findViewById(R.id.progressbar);
        pb.setVisibility(View.INVISIBLE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item1 = menu.add("GET");
        item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item1.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                MyHttpUtils.RequestData requestData = new MyHttpUtils.RequestData(URI_SHOW_PARAMS, "GET");

                requestData.setParameter("name", "Ali");
                requestData.setParameter("id", "09146894920");
                requestData.setParameter("age", "21");
                new Mytask().execute(requestData);

                return false;
            }
        });
        MenuItem item2 = menu.add("POST");
        item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                MyHttpUtils.RequestData requestData = new MyHttpUtils.RequestData(URI_SHOW_PARAMS, "POST");

                requestData.setParameter("name", "Ali");
                requestData.setParameter("number", "09146894920");
                requestData.setParameter("age", "21");
                new Mytask().execute(requestData);

                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public class Mytask extends AsyncTask<MyHttpUtils.RequestData, Void, String> {
        @Override
        protected void onPreExecute() {
            if (tasks.isEmpty()) {
                pb.setVisibility(View.VISIBLE);
            }
            tasks.add(this);
        }

        @Override
        protected String doInBackground(MyHttpUtils.RequestData... requestData) {
            MyHttpUtils.RequestData reqData = requestData[0];

            return MyHttpUtils.getDataHttpUrlConnection(reqData);
        }

        @Override
        protected void onPostExecute(String result) {
            if(result == null) {
                result = "null";
            }
            tv.setText(result);
            tasks.remove(this);
            if(tasks.isEmpty()){
                pb.setVisibility(View.INVISIBLE);
            }
        }
    }


}
