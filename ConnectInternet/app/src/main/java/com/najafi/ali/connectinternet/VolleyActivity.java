package com.najafi.ali.connectinternet;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class VolleyActivity extends AppCompatActivity {
    public static final String URI_SHOW_PARAMS = "http://goodnajafiboy.000webhostapp.com/mysite/showparams.php";
    ImageView imageView;
    RequestQueue requestQueue;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Wait ...");
        progressDialog.setMessage("");
        progressDialog.setCancelable(true);
        imageView = findViewById(R.id.imageView);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item1 = menu.add("GET");
        item1.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                sendParamsGet();
                return false;
            }
        });
        MenuItem item2 = menu.add("POST");
        item2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                sendParamsPost();
                return false;
            }
        });
        MenuItem item3 = menu.add("Image");
        item3.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        item3.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                loadImage();
                return false;
            }
        });


        return super.onCreateOptionsMenu(menu);
    }


    private void sendParamsGet() {
        Map<String, String> params = new HashMap<>();
        params.put("City", "Tabriz");
        params.put("Country", "Iran");
        String uri = URI_SHOW_PARAMS + "?" + MyHttpUtils.encodeParameters(params);

        final StringRequest request = new StringRequest(
                Request.Method.GET,
                uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        new AlertDialog.Builder(VolleyActivity.this)
                                .setTitle("Respose")
                                .setMessage(response)
                                .setPositiveButton("OK", null)
                                .show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        new AlertDialog.Builder(VolleyActivity.this)
                                .setTitle("Error")
                                .setMessage(error.getMessage())
                                .setPositiveButton("OK", null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();


                    }
                }

        );

        progressDialog.show();
        requestQueue.add(request);
    }

    private void sendParamsPost() {

        StringRequest request = new StringRequest(
                Request.Method.POST,
                URI_SHOW_PARAMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        new AlertDialog.Builder(VolleyActivity.this)
                                .setTitle("Respose")
                                .setMessage(response)
                                .setPositiveButton("OK", null)
                                .show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        new AlertDialog.Builder(VolleyActivity.this)
                                .setTitle("Error")
                                .setMessage(error.getMessage())
                                .setPositiveButton("OK", null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();


                    }
                }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("FirstName", "Ali");
                params.put("LastName", "Najafi");
                return params;
            }
        };

        progressDialog.show();
        requestQueue.add(request);
    }

    private void loadImage() {
        String uri = "https://goodnajafiboy.000webhostapp.com/mysite/photos/c.jpg";

        ImageRequest imageRequest = new ImageRequest(
                uri,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        imageView.setImageBitmap(response);
                        Toast.makeText(VolleyActivity.this, "DONE", Toast.LENGTH_SHORT).show();

                    }
                },
                imageView.getWidth(), imageView.getHeight(),
                ImageView.ScaleType.FIT_CENTER,
                Bitmap.Config.ARGB_8888,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(VolleyActivity.this, "Error" + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

        );
        requestQueue.add(imageRequest);
    }

}
