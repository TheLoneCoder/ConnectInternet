package com.najafi.ali.connectinternet;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AsyncTaskIntroActivity extends AppCompatActivity {


    TextView tv;
    int counter = 0;
    ProgressBar progressBar;
    List<TaskIntro> introTaskList = new ArrayList<>();
    List<TaskGetData> tasks = new ArrayList<>();
    public static final String SAMPLE_URL = "https://goodnajafiboy.000webhostapp.com/mysite/places.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task_intro);

        tv = findViewById(R.id.textView);

        tv.setMovementMethod(new ScrollingMovementMethod());
        progressBar = findViewById(R.id.progressBar);
        findViewById(R.id.btn_Intro).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TaskIntro("task #" + (++counter))
//                        .execute("bank", "hospital", "shopping");
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "bank", "hospital", "shopping");

            }
        });

        findViewById(R.id.btn_getData).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TaskGetData().execute(SAMPLE_URL);
            }
        });
    }

    public class TaskIntro extends AsyncTask<String, String, String> {


        String taskName;

        TaskIntro(String taskName) {
            this.taskName = taskName;

        }

        @Override
        protected void onPreExecute() {
            if (introTaskList.isEmpty()) {
                progressBar.setVisibility(View.VISIBLE);
            }
            tv.append("<" + taskName + "> : Start!\n");
            introTaskList.add(this);
        }

        @Override
        protected String doInBackground(String... strings) {

            for (String p : strings) {
                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress("working with param: " + p);
            }
            return "done\n";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            tv.append("<" + taskName + "> : " + values[0] + "\n");
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            tv.append("<" + taskName + "> : " + result);
            introTaskList.remove(this);
            if (introTaskList.isEmpty()) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Reset").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                tv.setText("");
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    public class TaskGetData extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            if (tasks.isEmpty()) {
                progressBar.setVisibility(View.VISIBLE);
            }
            tasks.add(this);
            tv.append("Getting Data ... ");
        }

        @Override
        protected String doInBackground(String... strings) {
            return MyHttpUtils.getDataHttpClient(strings[0]);
        }


        @Override
        protected void onPostExecute(String s) {
            tasks.remove(this);
            if (tasks.isEmpty()) {
                progressBar.setVisibility(View.INVISIBLE);
            }
            tv.append(s + "\n");
            super.onPostExecute(s);

        }
    }


}
